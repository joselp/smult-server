#include "utilities.h"
#include <strings.h>
#include <math.h>
#include <fstream>


#define LENGTH 512
int open_socket(){
    int socket = ::socket(PF_INET,SOCK_STREAM,0);
    if (socket == -1)
    {
     printf("Error al abrir socket\n");   
    }
    return socket;
}

void blint_to_port(int socket, int port){
    struct sockaddr_in name;
    name.sin_family      = PF_INET;
    name.sin_port        = (in_port_t) htons(port);
    name.sin_addr.s_addr = htonl(INADDR_ANY);
    
    int reuse = 1;
    if (setsockopt(socket, SOL_SOCKET, SO_REUSEADDR,(char *)&reuse, sizeof(int)) == -1 )
    {
        perror("No es posible reusar el socket\n");
    }

    int connect = bind(socket, (struct sockaddr*)&name, sizeof(name) );
    if(connect == -1)
    {
        perror("No se puede enlazar al puerto : dirección ya está en uso\n");
    }
}

int recv_file(int link, const char* path){

    // Se recibe un paquete de consulta
    printf("Se recibe del cliente: %i\n",link);

    //consult_package consult = {};
    //ssize_t d = recv(link, &consult,sizeof(consult), 0);
    char buffer_req[256];
    char buffer_Size_file[256];
    int n=0;
    ///SE LEE REQUEST PROVENIENTE DE CLIENTE
    
    std::fstream file; 
   
    file.open(path,std::ios::out | std::ios::app ); 
    int size;
    n = read(link,&size, sizeof(int));
    printf("size: %i\n",size);

    int Packs=(int)ceil((double)size/255.0);
    printf("packs: %i\n",Packs);
    char revbuf_req[LENGTH];

    for(int i; i<Packs; i++){
        bzero(buffer_req,256);
        n = 0;
        n = read(link, buffer_req, 255);
        // if (n < 0) //error("ERROR reading from socket");
        //printf("msg: %s\n",buffer_req);
        file<<buffer_req;

    }

    file.close();

}

int send_file(int link,const char* path){


    // SE ENVIA RESPUESTA

    char sdbuf_resp[LENGTH]; 

    char buffer_resp[256];


    //printf("Server Sending %s to the Client...\n ", fs_name);
    FILE *fs_resp = fopen(path, "r");
    if(fs_resp == NULL)
    {
        printf("ERROR: File %s not found.\n", path);
        exit(1);
    }

    int fs_block_sz;
    while((fs_block_sz = fread(sdbuf_resp, sizeof(char), LENGTH, fs_resp)) > 0)
    {
        if(send(link, sdbuf_resp, fs_block_sz, 0) < 0)
        {
            fprintf(stderr, "ERROR: Failed to send file %s. (errno = %d)\n", path, errno);
            break;
        }
        bzero(sdbuf_resp, LENGTH);
    }
    printf("Ok Response sended\n");

}

int fsize(FILE *fp){
    int prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    int sz=ftell(fp);
    fseek(fp,prev,SEEK_SET);
    return sz;
}

void create_dir(string path){
    string s_cmd = "mkdir -p " + path;
    const char* c_cmd  = s_cmd.c_str();
    system(c_cmd);
}

void rm_dir(string path){
    string s_cmd = "rm -rf " + path;
    const char* c_cmd  = s_cmd.c_str();
    system(c_cmd);
}


/*
void recv_file(int link){

    // Se recibe un paquete de consulta
    printf("Se recibe del cliente: %i\n",link);

    //consult_package consult = {};
    //ssize_t d = recv(link, &consult,sizeof(consult), 0);
    char buffer_req[256];
    char buffer_Size_file[256];
    int n=0;
    ///SE LEE REQUEST PROVENIENTE DE CLIENTE
    
    std::fstream file; 

    std::string fr_request_name = "tmp/request_";

    std::string link_str = std::to_string(link);

    std::string path =fr_request_name+link_str+".txt";

   
    file.open(path,std::ios::out | std::ios::app ); 
    int size;
    n = read(link,&size, sizeof(int));
    printf("size: %i\n",size);

    int Packs=(int)ceil((double)size/255.0);
    printf("packs: %i\n",Packs);
    char revbuf_req[LENGTH];

    for(int i; i<Packs; i++){
        bzero(buffer_req,256);
        n = 0;
        n = read(link, buffer_req, 255);
        // if (n < 0) //error("ERROR reading from socket");
        //printf("msg: %s\n",buffer_req);
        file<<buffer_req;

    }
    
    file.close();
}

void send_file(int link){

    //Nombre del archivo respuesta
    char const* fs_resp_name = "response.txt";

    char buffer_resp[256];


    //printf("Server Sending %s to the Client...\n ", fs_name);
    FILE *fs_resp = fopen(fs_resp_name, "r");
    if(fs_resp == NULL)
    {
        printf("ERROR: File %s not found.\n", fs_resp_name);
        exit(1);
    }

    int fs_block_sz;
    char sdbuf_resp[LENGTH]; 
    while((fs_block_sz = fread(sdbuf_resp, sizeof(char), LENGTH, fs_resp)) > 0)
    {
        if(send(link, sdbuf_resp, fs_block_sz, 0) < 0)
        {
            fprintf(stderr, "ERROR: Failed to send file %s. (errno = %d)\n", fs_resp_name, errno);
            break;
        }
        bzero(sdbuf_resp, LENGTH);
    }
    printf("Ok Response sended\n");
}
*/