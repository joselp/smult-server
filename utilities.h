
#if !defined(UTILITIES__H)
#define UTILITIES__H

#include <unistd.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <cstdio>
#include <cstring>
#include <string>
#include <vector>
#include <queue>
#include <sstream>
#include <iostream>

using namespace std;

int open_socket();

void blint_to_port(int socket, int port);
int recv_file(int link, const char* path);
int send_file(int link, const char* path);
int fsize(FILE *fp);

void create_dir(string path);
void rm_dir(string path);

#endif // UTILITIES__H
