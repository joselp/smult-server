all: build run

build:
	g++ -o server main.cpp server.cpp service.cpp  utilities.cpp  -pthread -std=c++11

run: build
	./server
