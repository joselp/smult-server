
#if !defined(SERVICE__H)
#define SERVICE__H

#include "utilities.h"
#include <queue>
#include <thread>
#include <iostream>

using namespace std;

#define PORT_CONSULT 8080

class Service
{
private:

public:
    int socket;
    int port;
    virtual void startup()            = 0;
    virtual void stop()               = 0;
    virtual void kill()               = 0;
};

class Consult : public Service
{
public:
    Consult(){};
    ~Consult(){};
    void startup();
    void comunicate(int link);
    void stop();
    void kill();
};


#endif // SERVICE__H
