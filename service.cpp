#include "service.h"
// Consult ************************************/
void Consult::startup(){

    this->port = PORT_CONSULT;
    int listener = open_socket();
    if(listener == -1){
        printf("Error en listern\n");
    }
    this->socket = listener;

    blint_to_port(listener, this->port);

    if (listen(listener, 10) == -1)
    {
        printf("No es posible escuchar en ese puerto\n");
    }
    
    printf("Enlazado al puerto\n");

    create_dir("consults");
    while (true)
    {
        int link;
        try
        {
            struct sockaddr_storage client = {};
            unsigned int addres_size = sizeof(client);
        
            printf("Esperando al cliente (Consult)\n");
            link = accept(listener, (struct sockaddr*)&client, &addres_size);
            if (link == -1)
            {
                printf("No se puede conectar socket secundario\n");
            }

            std::thread comunication(&Consult::comunicate,this,link);
            comunication.detach();
        }
        catch(const std::exception& e)
        {
            close(link);
        }
    }
}

void Consult::comunicate(int link){
    string link_path = "consults/" + to_string(link);
    rm_dir(link_path);
    create_dir(link_path);
    while (true)
    {

        // Path para almecenar el archivo a recibir
        string in_path = link_path + "/" + "consult.txt";
        const char* c_in_path = in_path.c_str();
        recv_file(link, c_in_path);
        
        // Path del archivo respuesta
        const char* c_out_path = "response.txt";
        send_file(link, c_out_path);       
    }   
}

void Consult::stop(){

}

void Consult::kill(){

}

