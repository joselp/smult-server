#if !defined(SERVER__H)
#define SERVER__H

#include "service.h"

class Server
{
public:
    Consult consults;
    Server(){};
    ~Server(){};
    void startup();
};

#endif // SERVER__H
